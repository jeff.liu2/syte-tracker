<?php
declare(strict_types=1);

namespace Syte\Tracker\Plugin\Quote\Model;

use Closure;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Session;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Item;
use Syte\Tracker\Helper\Data;
use Syte\Tracker\Model\Config;

class SetSyteTrackerOnCartAddPlugin
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * SetSyteTrackerOnCartAddPlugin constructor
     *
     * @param Config $config
     * @param Session $checkoutSession
     */
    public function __construct(
        Config $config,
        Session $checkoutSession
    ) {
        $this->config = $config;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Parses the product Qty data after update cart event.
     * In cases when product qty is increased the product data sets to session.
     *
     * @param Quote $subject
     * @param Closure $proceed
     * @param Product $product
     * @param $request
     * @param $processMode
     *
     * @return Item
     */
    public function aroundAddProduct(
        Quote   $subject,
        Closure $proceed,
        Product $product,
        $request = null,
        $processMode = null
    ): Item {
        $storeId = $subject->getStoreId();
        $qty = $request['qty'] ?? 0;
        $result = $proceed($product, $request, $processMode);

        if ($qty > $result->getQty()) {
            return $result;
        }

        $this->setItemForTriggerAddEvent($result, $storeId, $qty);

        return $result;
    }

    /**
     * Sets item data to session for triggering add event.
     *
     * @param Item $item
     * @param int $storeId
     * @param $qty
     *
     * @return void
     */
    private function setItemForTriggerAddEvent(Item $item, int $storeId, $qty)
    {
        if ($this->config->isServiceActive($storeId) && $this->config->isEcomEventActive($storeId)) {
            $this->checkoutSession->unsetData(Data::SYTE_TRACKER_SESSION_NAME);
            $this->checkoutSession->setData(Data::SYTE_TRACKER_SESSION_NAME, [
                [
                    'sku' => $item->getSku(),
                    'price' => $item->getPrice(),
                    'qty' => $qty,
                ]
            ]);
        }
    }
}
