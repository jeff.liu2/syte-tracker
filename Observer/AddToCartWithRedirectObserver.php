<?php
declare(strict_types=1);

namespace Syte\Tracker\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Exception\InputException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Syte\Tracker\Model\Config;
use Syte\Tracker\Helper\Data;
use Magento\Catalog\Model\Product;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Observer for handling Tracker related cookie
 *
 * Used for case when redirect to shopping cart is turned on
 */
class AddToCartWithRedirectObserver implements ObserverInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var Json
     */
    private $jsonHelper;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * AddToCartWithRedirectObserver constructor
     *
     * @param Config $config
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param Json $jsonHelper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Config $config,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        ScopeConfigInterface $scopeConfig,
        Json $jsonHelper,
        StoreManagerInterface $storeManager
    ) {
        $this->config = $config;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->scopeConfig = $scopeConfig;
        $this->jsonHelper = $jsonHelper;
        $this->storeManager = $storeManager;
    }

    /**
     * Send cookies after add to cart action
     *
     * @param Observer $observer
     *
     * @return void
     * @throws FailureToSendException If cookie couldn't be sent to the browser.
     * @throws CookieSizeLimitReachedException Thrown when the cookie is too big to store any additional data.
     * @throws InputException If the cookie name is empty or contains invalid characters.
     * @throws NoSuchEntityException If no store in store manager
     */
    public function execute(Observer $observer)
    {
        $store = $this->storeManager->getStore();
        if (
            !$this->config->isServiceActive($store->getId())
            || !$this->config->isEcomEventActive($store->getId())
            || !$this->isRedirectToCartEnabled()
        ) {
            return;
        }

        /** @var Product $product */
        $product = $observer->getEvent()->getProduct();
        $productsToAdd = [
            [
                'sku' => $product->getSku(),
                'price' => (float)$product->getPrice(),
                'qty' => $product->getQty(),
            ]
        ];

        $publicCookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
            ->setDuration(3600)
            ->setPath('/')
            ->setHttpOnly(false);

        $this->cookieManager->setPublicCookie(
            Data::SYTE_TRACKER_COOKIE_NAME,
            rawurlencode($this->jsonHelper->serialize($productsToAdd)),
            $publicCookieMetadata
        );
    }

    /**
     * Is redirect should be performed after the product was added to cart.
     *
     * @return bool
     */
    private function isRedirectToCartEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(
            'checkout/cart/redirect_to_cart',
            ScopeInterface::SCOPE_STORE
        );
    }
}
