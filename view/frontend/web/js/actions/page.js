define([
    'jquery'
], function ($) {
    'use strict';

    /**
     * Dispatch page visit event to Syte Tracker
     *
     * @private
     */
    function send() {
        var syteData = {
            name: 'fe_page_view',
            tag: 'ecommerce'
        };
        window.syteDataLayer.push(syteData);
    }

    return function () {
        send();
    };
});
