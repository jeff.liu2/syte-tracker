define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'underscore'
], function ($, customerData) {
    'use strict';

    /**
     * Getter for cookie
     *
     * @param {String} name
     */
    function getCookie(name) {
        var cookie = ' ' + document.cookie,
            search = ' ' + name + '=',
            setStr = null,
            offset = 0,
            end = 0;

        /* eslint-disable eqeqeq */
        /* eslint-disable max-depth */

        if (cookie.length > 0) {
            offset = cookie.indexOf(search);

            if (offset != -1) {
                offset += search.length;
                end = cookie.indexOf(';', offset);

                if (end == -1) {
                    end = cookie.length;
                }
                setStr = decodeURI(cookie.substring(offset, end));
            }
        }

        /* eslint-enable eqeqeq */
        /* eslint-enable max-depth */

        return setStr;
    }

    /**
     * Delete cookie
     *
     * @param {String} name
     */
    function delCookie(name) {
        var date = new Date(0);

        document.cookie = name + '=' + '; path=/; expires=' + date.toUTCString();
    }

    /**
     * Syte tracker universal cart class
     */
    function SyteTrackerUniversalCart() {
        this.cookieAddToCart = 'add_to_cart';
        this.productQtys = [];
        this.origProducts = {};
        this.productWithChanges = [];
        this.addedProducts = [];
        this.syteTrackingUniversalData = {};
    }

    SyteTrackerUniversalCart.prototype = {
        /**
         * Listen mini cart reload
         */
        listenMinicartReload: function () {
            var context = this;

            if (!_.isUndefined(window.Minicart) && typeof Minicart.prototype.initAfterEvents) {
                Minicart.prototype.initAfterEvents['SyteTrackerUniversalCart:subscribeProductsUpdateInCart'] =

                    /**
                     * Wrapper function
                     */
                    function () {
                        context.subscribeProductsUpdateInCart();
                        context.parseAddToCartCookies();
                    };
            }
        },

        /**
         * Subscribe products update in cart
         */
        subscribeProductsUpdateInCart: function () {
            var context = this;

            $(document)
                .on('mousedown', '[data-cart-item-update]', function () {
                    context.collectCustomerProducts();
                })
                .on('mousedown', '.update-cart-item', function () {
                    context.collectCustomerProducts();
                })
                .on('mousedown', '[data-multiship-item-update]', function () {
                    context.collectOriginalProducts();
                    context.collectMultiCartQtys();
                })
                .on('ajax:updateCartItemQty', function () {
                    context.updateCartObserver();
                })
                .on('ajax:updateMulticartItemQty', function () {
                    context.updateMulticartCartObserver();
                });
        },

        /**
         * Update multi cart observer
         */
        updateMulticartCartObserver: function () {
            this.collectMultiProductsWithChanges();
            this.collectProductsForMessages();
            this.cartItemAdded();
        },

        /**
         * Update cart observer
         */
        updateCartObserver: function () {
            this.collectProductsWithChanges();
            this.collectProductsForMessages();
            this.cartItemAdded();
        },

        /**
         * Collect multi products with changes
         */
        collectMultiProductsWithChanges: function () {
            var groupedProducts = {},
                cartProduct,
                i = 0,
                j,
                product;

            this.productWithChanges = [];

            for (i; i < this.productQtys.length; i++) {
                cartProduct = this.productQtys[i];

                if (_.isUndefined(groupedProducts[cartProduct.id])) {
                    groupedProducts[cartProduct.id] = parseInt(cartProduct.qty, 10);
                } else {
                    groupedProducts[cartProduct.id] += parseInt(cartProduct.qty, 10);
                }
            }

            /* eslint-disable max-depth */
            /* eslint-disable eqeqeq */
            for (j in groupedProducts) {

                if (groupedProducts.hasOwnProperty(j)) {

                    if (!_.isUndefined(this.origProducts[j]) && groupedProducts[j] != this.origProducts[j].qty) {
                        product = $.extend({}, this.origProducts[j]);
                        product.qty = groupedProducts[j];
                        this.productWithChanges.push(product);
                    }
                }
            }

            /* eslint-enable max-depth */
            /* eslint-enable eqeqeq */
        },

        /**
         * Collect products with changes
         */
        collectProductsWithChanges: function () {
            var i = 0,
                cartProduct,
                product;

            this.productWithChanges = [];

            /* eslint-disable eqeqeq */
            /* eslint-disable max-depth */
            for (i; i < this.productQtys.length; i++) {
                cartProduct = this.productQtys[i];

                if (
                    !_.isUndefined(this.origProducts[cartProduct.id]) &&
                    cartProduct.qty != this.origProducts[cartProduct.id].qty
                ) {
                    product = $.extend({}, this.origProducts[cartProduct.id]);

                    if (parseInt(cartProduct.qty, 10) > 0) {
                        product.qty = cartProduct.qty;
                        this.productWithChanges.push(product);
                    }
                }
            }
            /* eslint-enable max-depth */
            /* eslint-enable eqeqeq */
        },

        /**
         * Retrieves data about added products.
         */
        collectCustomerProducts: function () {
            this.collectOriginalProducts();
            this.collectCartQtys();
            this.collectMiniCartQtys();
        },

        /**
         * Collect original products
         */
        collectOriginalProducts: function () {
            var products = {},
                items = customerData.get('cart')().items;

            if (!_.isUndefined(items)) {
                items.forEach(function (item) {
                    products[item['product_sku']] = {
                        'id': item['product_sku'],
                        'sku': item['product_sku'],
                        'price': item['product_price_value'],
                        'qty': parseInt(item.qty, 10)
                    };
                });
            }

            this.syteTrackingUniversalData.shoppingCartContent = products;
            this.origProducts = this.syteTrackingUniversalData.shoppingCartContent;
        },

        /**
         * Collect multi cart qtys
         */
        collectMultiCartQtys: function () {
            var productQtys = [];

            $('[data-multiship-item-id]').each(function (index, elem) {
                productQtys.push({
                    'id': $(elem).data('multiship-item-id'),
                    'qty': $(elem).val()
                });
            });

            this.productQtys = productQtys;
        },

        /**
         * Collect cart qtys
         */
        collectCartQtys: function () {
            var productQtys = [];

            $('[data-cart-item-id]').each(function (index, elem) {
                productQtys.push({
                    'id': $(elem).data('cart-item-id'),
                    'qty': $(elem).val()
                });
            });

            this.productQtys = productQtys;
        },

        /**
         * Collect mini cart qtys
         */
        collectMiniCartQtys: function () {
            var productQtys = [];

            $('input[data-cart-item-id]').each(function (index, elem) {
                productQtys.push({
                    'id': $(elem).data('cart-item-id'),
                    'qty': $(elem).val()
                });
            });

            this.productQtys = productQtys;
        },

        /**
         * Collect products for messages
         */
        collectProductsForMessages: function () {
            var i = 0,
                product;

            this.addedProducts = [];
            /* eslint-disable max-depth */
            for (i; i < this.productWithChanges.length; i++) {
                product = this.productWithChanges[i];
                if (!_.isUndefined(this.origProducts[product.id])) {

                    if (product.qty > this.origProducts[product.id].qty) {
                        product.qty = Math.abs(product.qty - this.origProducts[product.id].qty);
                        this.addedProducts.push(product);
                    }
                }
            }
            /* eslint-enable max-depth */
        },

        /**
         * Format products array
         *
         * @param {Object} productsIn
         */
        formatProductsArray: function (productsIn) {
            var productsOut = [],
                itemId,
                i;

            /* eslint-disable max-depth */
            /* eslint-disable eqeqeq */
            for (i in productsIn) {

                if (i != 'length' && productsIn.hasOwnProperty(i)) {

                    if (!_.isUndefined(productsIn[i].sku)) {
                        itemId = productsIn[i].sku;
                    } else {
                        itemId = productsIn[i].id;
                    }

                    productsOut.push({
                        'id': itemId,
                        'sku': itemId,
                        'price': productsIn[i].price,
                        'quantity': parseInt(productsIn[i].qty, 10)
                    });
                }
            }

            /* eslint-enable max-depth */
            /* eslint-enable eqeqeq */

            return productsOut;
        },

        /**
         * Cart item add action
         */
        cartItemAdded: function () {
            if (!this.addedProducts.length) {
                return;
            }

            var syteData = {
                name: 'products_added_to_cart',
                tag: 'ecommerce',
                products: this.formatProductsArray(this.addedProducts)
            };
            window.syteDataLayer.push(syteData);

            this.addedProducts = [];
        },

        /**
         * Add to cart action
         *
         * @param {String} id
         * @param {String} price
         * @param {String} quantity
         */
        addToCartAjax: function (id, price, quantity) {
            var syteData = {
                name: 'products_added_to_cart',
                tag: 'ecommerce',
                products: [{
                    sku: id,
                    quantity: quantity,
                    price: price
                }]
            };
            window.syteDataLayer.push(syteData);
        },

        /**
         * Parse add from cart cookies.
         */
        parseAddToCartCookies: function () {
            var addProductsList;

            if (getCookie(this.cookieAddToCart)) {
                this.addedProducts = [];
                addProductsList = decodeURIComponent(getCookie(this.cookieAddToCart));
                this.addedProducts = JSON.parse(addProductsList);
                delCookie(this.cookieAddToCart);
                this.cartItemAdded();
            }
        }
    };

    return SyteTrackerUniversalCart;
});
